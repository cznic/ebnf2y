ebnf2y
======

Command ebnf2y converts EBNF grammars into yacc compatible skeleton .y files.

Installation:

    $ go get modernc.org/ebnf2y

Documentation: [http://godoc.org/modernc.org/ebnf2y](http://godoc.org/modernc.org/ebnf2y)
