module modernc.org/ebnf2y

require (
	golang.org/x/exp v0.0.0-20181106170214-d68db9428509
	modernc.org/ebnfutil v1.0.0
	modernc.org/strutil v1.0.0
)
